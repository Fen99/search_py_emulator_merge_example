#pragma once

#include <cstdint>

namespace search_lib {

uint64_t MurmurHash64(const void* key, std::size_t len, uint64_t seed = 0);

} // namespace search_lib
