#pragma once

#include <memory>
#include <string>
#include <string_view>
#include <vector>

class CLemmatizer;

namespace search_lib {

class Lemmatizer
{
public:
    Lemmatizer(std::string aot_path); // third-party/aot; /Dicts/Morph/Russian должны быть собраны
    ~Lemmatizer();
    Lemmatizer(const Lemmatizer&) = delete;
    Lemmatizer& operator=(const Lemmatizer&) = delete;

    // <input> (аргумент) - 1 слово в UTF-8 в любом регистре
    // Возвращает список возможных лемматизаций
    std::vector<std::string> Lemmatize(std::string_view);

    // Прочие методы нормализации
    static std::string ToUpper(std::string_view);

private:
    class Impl;
    std::unique_ptr<CLemmatizer> impl_;
};

} // namespace search_lib
