#include "text_process/lemmatizer.hpp"

#include "lemmatizer_base_lib/Lemmatizers.h"

namespace search_lib {

Lemmatizer::Lemmatizer(std::string aot_path) :
    impl_(std::make_unique<CLemmatizerRussian>())
{
    aot_path += "/Dicts/Morph/Russian";
    impl_->LoadDictionariesFromPath(aot_path);
}

Lemmatizer::~Lemmatizer()
{}

std::vector<std::string> Lemmatizer::Lemmatize(std::string_view inp)
{
    std::string word_s8 = convert_from_utf8(inp.data(), MorphLanguageEnum::morphRussian);
	bool capital = is_upper_alpha((BYTE)word_s8[0], MorphLanguageEnum::morphRussian);

    std::vector<CFormInfo> paradigms;
    impl_->CreateParadigmCollection(false, word_s8, capital, true, paradigms);

    std::vector<std::string> result;
    result.reserve(paradigms.size());
    std::transform(paradigms.begin(), paradigms.end(), std::back_inserter(result),
                   [](const CFormInfo& fi) { return fi.GetWordFormUtf8(0); });
    return result;
}

std::string Lemmatizer::ToUpper(std::string_view inp)
{
    std::string word_s8 = convert_from_utf8(inp.data(), MorphLanguageEnum::morphRussian);
    for (std::size_t i = 0; i < word_s8.size(); i++)
    {
        if (is_english_lower(word_s8[i]))
            word_s8[i] = ReverseChar(word_s8[i], MorphLanguageEnum::morphEnglish);
        else if (is_russian_lower(word_s8[i]))
            word_s8[i] = ReverseChar(word_s8[i], MorphLanguageEnum::morphRussian);
    }

    return convert_to_utf8(word_s8, MorphLanguageEnum::morphRussian);
}

} // namespace search_lib
