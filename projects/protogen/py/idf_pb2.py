# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: idf.proto
# Protobuf Python Version: 4.26.0-dev
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\tidf.proto\x12\nsearch_lib\"B\n\nIdfStorage\x12\x13\n\x0b\x64\x65\x66\x61ult_idf\x18\x01 \x01(\x02\x12\x11\n\tterm_hash\x18\x02 \x03(\x04\x12\x0c\n\x04idfs\x18\x03 \x03(\x02\x62\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'idf_pb2', _globals)
if _descriptor._USE_C_DESCRIPTORS == False:
  DESCRIPTOR._loaded_options = None
  _globals['_IDFSTORAGE']._serialized_start=25
  _globals['_IDFSTORAGE']._serialized_end=91
# @@protoc_insertion_point(module_scope)
