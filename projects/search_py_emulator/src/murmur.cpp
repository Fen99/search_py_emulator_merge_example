#include "text_process/murmurhash.hpp"

#include <cassert>
#include <cstring>
#include <iostream>
#include <string>

using namespace search_lib;

extern "C" {

std::uint64_t MurMurHash(const char* str)
{
    return MurmurHash64(str, std::strlen(str));
}

} // extern "C"
