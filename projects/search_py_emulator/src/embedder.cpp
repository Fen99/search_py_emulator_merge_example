#include "text_process/neuro_predict.hpp"

#include <cassert>
#include <memory>
#include <vector>

using namespace search_lib;

extern "C" {

TextEmbedder* CreateEmbedder(const char* spiece_path, const char* model_path)
{
    auto embedder = std::make_unique<TextEmbedder>(spiece_path, model_path);
    return embedder.release();
}

float* EmbedText(TextEmbedder* embedder, const char* text)
{
    static std::vector<float> result;

    assert(embedder);
    result = embedder->Encode(text);
    return result.data();
}

std::uint32_t EmbedingSize(TextEmbedder* embedder)
{
    return embedder->EmbedingSize();
}

void DestroyEmbedder(TextEmbedder* embedder)
{
    delete embedder;
}

} // extern "C"
