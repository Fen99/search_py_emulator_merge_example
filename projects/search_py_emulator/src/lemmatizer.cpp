#include "text_process/lemmatizer.hpp"

#include <cassert>
#include <memory>
#include <string>

using namespace search_lib;

extern "C" {

Lemmatizer* CreateLemmatizer(const char* aot_path) // <repo>/third-party/aot
{
    auto lemmatizer = std::make_unique<Lemmatizer>(aot_path);
    return lemmatizer.release();
}

const char* Lemmatize(Lemmatizer* lemm, const char* word)
{
    static std::string result;

    assert(lemm);
    result = lemm->Lemmatize(word)[0]; // Возвращаем 1й из результатов лемматизации
    return result.c_str();
}

void DestroyLemmatizer(Lemmatizer* lemm)
{
    delete lemm;
}

} // extern "C"
