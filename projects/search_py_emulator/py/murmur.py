#!/usr/bin/env python3

import ctypes

def init_so(so_object):
    so_object.MurMurHash.argtypes = [ctypes.c_char_p]
    so_object.MurMurHash.restype = ctypes.c_uint64

class Instance:
    def __init__(self, so_object):
        self.so_object = so_object
        pass

    def deinit(self):
        pass

    def __call__(self, text):
        return self.so_object.MurMurHash(text.encode('utf-8') if type(text) == str else text)
