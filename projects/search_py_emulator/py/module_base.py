#!/usr/bin/env python3

class ModuleBase:
    def __init__(self, so_object, ptr, deinit_func):
        self.so_object = so_object

        if ptr is None:
            raise RuntimeError('Invalid loading!')

        self.ptr = ptr
        self.deinit_func = deinit_func

    def deinit(self):
        self.deinit_func(self.ptr)
