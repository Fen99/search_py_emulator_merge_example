#!/usr/bin/env python3

import murmur
import lemmatizer
import embedder

import argparse
import ctypes
import math
import sys


parser = argparse.ArgumentParser()
parser.add_argument('root', type=str, help='repo root')
args = parser.parse_args()


class Controller:
    def __init__(self, root):
        self.so_object = ctypes.CDLL(root + '/bin/libsearch_py_emulator_lib.so')

        lemmatizer.init_so(self.so_object)

        self.root = root
        self.lemm_instance = None
        self.murmur_instance = None
        self.emb_instance = None

    def __enter__(self):
        self.lemm_instance = lemmatizer.Instance(self.so_object, (self.root + '/third-party/aot').encode('utf-8'))
        self.emb_instance = embedder.Instance(so_object, self.root + '/data/e5/sentencepiece.bpe.model', self.root + '/data/e5/traced_e5.pt')
        self.murmur_instance = murmur.Instance(self.so_object)
        return self

    def __exit__(self, type, value, traceback):
        self.lemm_instance.deinit()
        self.murmur_instance.deinit()
        self.emb_instance.deinit()


with Controller(args.root) as ctrl:
    for line in sys.stdin:
        line = line.strip()
        if not line:
            continue

        src_mm = ctrl.murmur_instance(line)
        lemm_mm = ctrl.murmur_instance(ctrl.lemm_instance(line))

        src_emb = emb_instance(line)
        lemm_emb = emb_instance(lemm_instance(line))

        assert len(src_emb) == len(lemm_emb)

        cos_dist = 0
        norm_src = 0
        norm_lem = 0
        for sc, lc in zip(src_emb, lemm_emb):
            cos_dist += sc * lc
            norm_src += sc * sc
            norm_lem += lc * lc

        cos_dist /= (math.sqrt(norm_src) * math.sqrt(norm_lem))

        print(f'Src = {line}, emb = {ctrl.lemm_instance(line).decode("utf-8")}, dist = {abs(src_mm - lemm_mm)}, cos dist = {cos_dist}')

