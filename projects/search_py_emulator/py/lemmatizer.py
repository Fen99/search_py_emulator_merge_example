#!/usr/bin/env python3

from module_base import ModuleBase

import ctypes

def init_so(so_object):
    so_object.CreateLemmatizer.argtypes = [ctypes.c_char_p]
    so_object.CreateLemmatizer.restype = ctypes.c_void_p

    so_object.Lemmatize.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
    so_object.Lemmatize.restype = ctypes.c_char_p

    so_object.DestroyLemmatizer.argtypes = [ctypes.c_void_p]
    so_object.DestroyLemmatizer.restype = None

class Instance(ModuleBase):
    def __init__(self, so_object, aot_path):
        super().__init__(so_object, so_object.CreateLemmatizer(aot_path.encode('utf-8')), so_object.DestroyLemmatizer)

    def __call__(self, text):
        return self.so_object.Lemmatize(self.ptr, text.encode('utf-8')).decode('utf-8')
