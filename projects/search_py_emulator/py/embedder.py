#!/usr/bin/env python3

from module_base import ModuleBase

import ctypes

def init_so(so_object):
    so_object.CreateEmbedder.argtypes = [ctypes.c_char_p, ctypes.c_char_p]
    so_object.CreateEmbedder.restype = ctypes.c_void_p

    so_object.EmbedText.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
    so_object.EmbedText.restype = ctypes.POINTER(ctypes.c_float)

    so_object.EmbedingSize.argtypes = [ctypes.c_void_p]
    so_object.EmbedingSize.restype = ctypes.c_uint32

    so_object.DestroyEmbedder.argtypes = [ctypes.c_void_p]
    so_object.DestroyEmbedder.restype = None

class Instance(ModuleBase):
    def __init__(self, so_object, spiece_path, model_path):
        super().__init__(so_object, so_object.CreateEmbedder(spiece_path, model_path), so_object.DestroyEmbedder)
        self.embedding_size = self.so_object.EmbedingSize(self.ptr)

    def __call__(self, text):
        result = []

        emb_ptr = self.so_object.EmbedText(self.ptr, text.encode('utf-8'))
        for i in range(self.embedding_size):
            result.append(emb_ptr[i])
        return result
